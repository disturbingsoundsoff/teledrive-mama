import {useState} from 'react';
import logo from './assets/images/logo-universal.png';
import './App.css';
import {Greet, HelloKolya} from "../wailsjs/go/main/App";

function App() {
    const [resultText, setResultText] = useState("Please enter your name below 👇");
    const [name, setName] = useState("");

    const [message, setMessage] = useState("")
    const [messageToKolya, setMessageToKolya] = useState("")

    const updateName = (e: any) => setName(e.target.value);
    const updateResultText = (result: string) => setResultText(result);


    function greet() {
        Greet(name).then(updateResultText);
    }

    // function updateKolyaMessage() {
    //     HelloKolya(message).then(message => setMessageToKolya(message))
    // }

    async function updateKolyaMessage() {
        const rez = await HelloKolya(message);
        setMessageToKolya(rez);
    }

    return (
        <div id="App">
            <div id="result" className="result">{resultText}</div>
            <div id="input" className="input-box">
                <input id="name" className="input" onChange={ e => setName(e.target.value)} autoComplete="off" name="input" type="text"/>
                <button className="btn" onClick={greet}>Greet</button>
            </div>

            <br /><br />

            <div style={{display: "flex", flexDirection: "column", alignItems: "center"}}>
                <div style={{display: "flex"}}>
                    <input type="text" onChange={ e => setMessage(e.target.value)}/>
                    <span style={{width: "20px"}}></span>
                    <button onClick={updateKolyaMessage}>Фраза готова</button>
                </div>
                <span>{messageToKolya}</span>
            </div>


        </div>
    )
}

export default App
